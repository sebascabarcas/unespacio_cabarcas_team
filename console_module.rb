require_relative 'scraper_module'
require 'console_view_helper'

module ConsoleModule
	@@params = {}
 	#For get user information
	def self.get_info
	  	puts "Ingrese su usuario uninorte"
	  	user = ConsoleViewHelper.input
	  	params["sid"] = user
	  	pass = ConsoleViewHelper.hidden_input
	  	params["PIN"] = pass
	end
	#Room's params
	def self.get_room
		puts "Room Number"
	  	params["room"] = ConsoleViewHelper.input
	  	puts "Duration (30, 60, 90 ...)"
	  	params["duration"] = ConsoleViewHelper.input
	  	puts "Month (0, 1, 2 ...)"
	  	params["month"] = ConsoleViewHelper.input
	  	puts "Day"
	  	params["day"] = ConsoleViewHelper.input
	end
	def self.get_time
		puts "Time"
	  	params["time"] = ConsoleViewHelper.input
	end
	 #Allows show an array in a table form
	def self.show_table(array)
	  puts  ConsoleViewHelper.table(array)
	end
	def self.params
		@@params
	end
end
