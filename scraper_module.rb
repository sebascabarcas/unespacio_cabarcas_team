require 'mechanize'
require 'watir'
require 'phantomjs'
require_relative 'console_module'

module ScraperModule
  @@url =  {
    pomelo:"https://pomelo.uninorte.edu.co/pls/prod/twbkwbis.P_ValLogin",
    unespacio:"http://guaymaro.uninorte.edu.co/UNEspacio/index.php?p=Index",
    find_a_room:"http://guaymaro.uninorte.edu.co/UNEspacio/index.php?p=FindRoomSS&r=1",
    horario_pomelo: "https://pomelo.uninorte.edu.co/pls/prod/bwskfshd.P_CrseSchdDetl"
  }

  @@rooms = {}
  @@time = {}

  def self.create_bot
    return Mechanize.new
  end

  def self.url
   return @@url
  end

  #Return the robot with the unespacio credentials
  def self.login_unespacio(url, params)
    #phantomjs
    browser = ::Watir::Browser.new :phantomjs
    browser.goto(url)
    browser.li(class:"userBarButton ", index:1).click
    form = browser.form(id:"frmLogin")
    browser.text_field(name:'txtUsername').set(params["sid"])
    browser.text_field(name: 'txtPassword').set(params["PIN"])
    browser.button(id: 'btnLogin').click
    browser.refresh
    p browser.element(css: '#spanUsername').present?
    p browser.element(css: '#spanUsername').text.strip
    browser
  end

  def self.login_pomelo(url, params)
    browser = ::Watir::Browser.new :firefox
    browser.goto(url)
    browser.text_field(name: 'sid').set(params["sid"])
    browser.text_field(name: 'PIN').set(params["PIN"])
    browser.element(css: 'body > div.pagebodydiv > form > p > input[type="submit"]').click
    browser
  end

  def self.horario_pomelo(url, browser)
    browser.goto(url)
    browser.select_list(:id, 'term_id').select('201730')
    browser.element(css: 'body > div.pagebodydiv > form > input[type="submit"]').click
    # page_html = parse_html(browser)
    # page_html = page_html.css('.ddlabel > a')
    # page_html.each { |n| puts n.content }
  end

  #Print info of rooms
  def self.name_rooms(browser)
    browser.element(css: '#menuContent > ul > li:nth-child(2) > ul > li:nth-child(3) > div > a').click
    browser.select_list(:id, 'cboLocation').select('B_58468bae-fafe-43eb-98de-f7b992f1ac2b')
    page_html = parse_html(browser)
    i = 1
    sw = true
    while sw do
      if (page_html.xpath('//*[@id="ajaxRoomList"]/div[3]/table/tbody/tr[' + i.to_s + ']/td[3]').text != "")
          puts i.to_s + ') ' + page_html.xpath('//*[@id="ajaxRoomList"]/div[3]/table/tbody/tr[' + i.to_s + ']/td[3]').text
          @@rooms[i.to_s] = page_html.xpath('//*[@id="ajaxRoomList"]/div[3]/table/tbody/tr[' + i.to_s + ']/td[5]/span/a').first.attributes['onclick'].content.split("'")[1].strip
          i +=1
      else
        if(browser.element(css: '#ajaxRoomList > div.ListActionBar.ListActionBarBottom > div.PageSelectorControls > input[type="number"]').value.to_s == "1")
          browser.element(css: '#ajaxRoomList > div:nth-child(2) > div.PageSelectorControls > div.imgNextArrow').click
          page_html_2 = parse_html(browser)
          j = 1
          while (page_html_2.xpath('//*[@id="ajaxRoomList"]/div[3]/table/tbody/tr[' + j.to_s + ']/td[3]').text != "") do
            puts i.to_s + ') ' + page_html_2.xpath('//*[@id="ajaxRoomList"]/div[3]/table/tbody/tr[' + j.to_s + ']/td[3]').text
            @@rooms[i.to_s] = page_html_2.xpath('//*[@id="ajaxRoomList"]/div[3]/table/tbody/tr[' + j.to_s + ']/td[5]/span/a').first.attributes['onclick'].content.split("'")[1].strip
            i +=1
            j+= 1
          end
          browser.element(css: '#ajaxRoomList > div:nth-child(2) > div.PageSelectorControls > div.imgBackArrow').click
          sw = false
        end
      end
    end
    browser
  end

  def self.parse_html(browser)
    sleep 1
    Nokogiri::HTML.parse(browser.html)
  end

  def self.booking(browser, params)
    if(browser.element(css: '#ajaxRoomList > div.listDiv.initialized > table > tbody > tr:nth-child(' + params["room"].to_s + ')').present?)
      browser.element(css: '#ajaxRoomList > div.listDiv.initialized > table > tbody > tr:nth-child(' + params["room"].to_s + ')').click
    else
      browser.element(css: '#ajaxRoomList > div:nth-child(2) > div.PageSelectorControls > div.imgNextArrow').click
      room = params["room"].to_i - 30
      browser.element(css: '#ajaxRoomList > div.listDiv.initialized > table > tbody > tr:nth-child(' + room.to_s + ')').click
    end
    browser.select_list(:id, 'cboDuration').select(params["duration"].to_s)
    browser.element(css: '#btnAnyDate').click
    browser = search_day(browser, params["day"], params["month"])
    page_html = parse_html(browser)
    i = 1
    while i < 29 do
      date = page_html.xpath('//*[@id="roomavailability"]/div/div/div[2]/table/tbody/tr[' + i.to_s + ']/td[1]').text
      if (page_html.at_css('#roomavailability > div > div > div.listDiv.initialized.RoomAvailabilityList.NoPadding.NoEntityType.TableLayoutAuto > table > tbody > tr:nth-child(' + i.to_s + ') > td:nth-child(2) > input'))
          puts i.to_s + ') ' + date.to_s + ' >> Room is available'
          @@time[i.to_s] = browser.element(css: '#roomavailability > div > div > div.listDiv.initialized.RoomAvailabilityList.NoPadding.NoEntityType.TableLayoutAuto > table > tbody > tr:nth-child(' + i.to_s + ') > td:nth-child(2) > input')
      else
          puts i.to_s + ') ' + date.to_s + ' >> Room is unavailable'
      end
      i += 1
    end
    browser
  end

  def self.search_day(browser, day, month)
    sw = true
    i = 3
    while sw && i <= 7 do
      j = 1
      while j <= 7 do
        clickeable = browser.element(css: '#availabilityCalendar' + month.to_s + ' > table > tbody > tr:nth-child(' + i.to_s + ') > td:nth-child(' + j.to_s + ')')
        if(clickeable.text.to_s == day.to_s)
          sw = false
          clickeable.click
        end
        j += 1
      end
      i += 1
    end
    browser
  end

  def self.confirm_booking(browser, params)
    @@time[params["time"]].click
    browser.element(css: '#btnConfirm').click
    browser.element(css: 'body > div.MessageBoxWindow > div.MessageBoxButtons.NoBorder > input:nth-child(1)').click
    browser.element(css: 'input.MessageBoxButton:nth-child(1)').click
    browser
  end

end